// Implement your react app here

import React, { useEffect, useState } from 'react';
import { Table } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from 'axios';
import moment from 'moment';

export default function App() {
    const [data, setData] = useState([])
    const [pages, setPages] = useState(0)
    const [page, setPage] = useState(1)
    const [paginationData, setPaginationData] = useState([])
    const [originalData, setOriginalData] = useState([])
    const [sortType, setSortType] = useState('asc')

    const sortByName = () => {
        try {
            let sortedData = paginationData
            if (sortType == 'asc') {
                sortedData = paginationData.sort((a, b) => (a.GivenName > b.GivenName) ? 1 : ((b.GivenName > a.GivenName) ? -1 : 0))
                setSortType('des')
            } else {
                sortedData = paginationData.sort((a, b) => (a.GivenName < b.GivenName) ? 1 : ((b.GivenName < a.GivenName) ? -1 : 0))
                setSortType('asc')
            }
            setPaginationData(sortedData)
            paginate(page)
        } catch (ex) {
            console.log(ex);
        }
    }

    const filterData = (e) => {
        try {
            if (e.target.value.length == 0) {
                paginate()
                return
            }
            let filteredData = []
            filteredData = originalData.filter(person =>
                person.GivenName.toLowerCase().includes(e.target.value.toLowerCase())
            )
            setPaginationData(filteredData)
            paginate(page)
        } catch (ex) {
            console.log(ex);
        }
    }

    const deletePerson = (id) => {
        try {
            let newData = paginationData
            newData.splice(newData.findIndex(person =>
                person._id === id
            ), 1)
            setPaginationData(newData)
            setPages(newData.length / 10 + (newData.length % 10 > 0 ? 1 : 0))
            paginate(page)
        } catch (ex) {
            console.log(ex);
        }
    }

    const paginate=(pg=1,dt=paginationData)=>{
        let pageData=[]
        let end=dt.length<(pg*10)?dt.length:(pg*10)
        for(let i=((pg*10)-10);i<end;i++){
            pageData.push(dt[i])
        }
        setData(pageData)
        setPage(pg)
        if(pageData.length==0)
        {
            if(page>1){
                paginate(page-1)
            }
        }
    }

    useEffect(() => {
        try {
            axios.get(`http://localhost:80/api/personnel`)
                .then(res => {
                    setOriginalData(res.data)
                    setPaginationData(res.data)
                    setPages(res.data.length / 10 + (res.data.length % 10 > 0 ? 1 : 0))
                    paginate(page,res.data)
                })
                .catch(x => { console.log(x) })
        } catch (ex) {
            console.log(ex);
        }
    }, [])
    return (
        <>
            <div style={{ margin: "50px 50px" }}>
                <input placeholder='Search here...' onChange={(e) => filterData(e)} />

                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th style={{ color: 'blue', cursor: "pointer" }} onClick={() => sortByName()}>Name</th>
                            <th>Family Name</th>
                            <th>Reference</th>
                            <th>DOB</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            data.length > 0 ? data.map((person, index) => {
                                return (
                                    <tr key={index}>
                                        <td>{person?._id}</td>
                                        <td>{person?.GivenName}</td>
                                        <td>{person?.FamilyName}</td>
                                        <td>{person?.Reference}</td>
                                        <td>{moment(person?.DateOfBirth).format("YYYY/MM/DD kk:mm:ss")}</td>
                                        <td style={{ color: 'red', cursor: "pointer" }} onClick={() => deletePerson(person?._id)}>Delete</td>
                                    </tr>
                                )
                            })
                                :
                                <tr>
                                    <td colSpan={6}>Data not found</td>
                                </tr>
                        }
                    </tbody>
                </Table>
                <nav aria-label="Page navigation example">
                    <ul className="pagination">
                        <li className="page-item"><a className="page-link" href="#">Previous</a></li>
                        {(() => {
                            const pagesArray = [];
                            for (let i = 1; i <= pages; i++) {
                                pagesArray.push(<li key={i} className="page-item" onClick={()=>paginate(i)}><a style={{background:page==i?'orange':'',color:page==i?'white':'blue'}} href="#" className="page-link">{i}</a></li>);
                            }
                            return pagesArray;
                        })()}
                        <li className="page-item"><a className="page-link" href="#">Next</a></li>
                    </ul>
                </nav>

            </div>
        </>
    )
}