// Implement your web server here

const express = require('express');

const PORT = 80;
const app = express();

app.use(function(req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:1234')
  next();
})

const fs = require('fs');
const path = require('path');

let rawdata = fs.readFileSync(path.resolve('./server/data/', 'personnel.json'));
let personnel = JSON.parse(rawdata);

app.get('/api/personnel', function(req, res) {
    res.json(personnel);
});

app.listen(PORT, () => console.log(`App running at http://localhost on port ${PORT}`));